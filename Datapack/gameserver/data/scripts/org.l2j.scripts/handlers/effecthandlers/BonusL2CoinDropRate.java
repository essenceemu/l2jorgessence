package handlers.effecthandlers;

import org.l2j.gameserver.model.StatsSet;
import org.l2j.gameserver.model.stats.Stats;

public class BonusL2CoinDropRate extends AbstractStatEffect {
    public BonusL2CoinDropRate(StatsSet params)
    {
        super(params, Stats.BONUS_L2COIN_DROP_RATE);
    }
}
