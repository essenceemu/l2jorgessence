package org.l2j.gameserver.enums;

public enum ItemPackageType {
    Default,
    BasicInfo,
    PagedItemInfo
}
