package org.l2j.gameserver.network.clientpackets;

import org.l2j.gameserver.Config;
import org.l2j.gameserver.data.xml.impl.TeleportListData;
import org.l2j.gameserver.data.xml.model.TeleportListInfo;
import org.l2j.gameserver.model.items.CommonItem;
import org.l2j.gameserver.network.SystemMessageId;

public class RequestExRequestTeleport extends ClientPacket {
    private int id;

    @Override
    protected void readImpl() throws Exception {
        id = readInt();
    }

    @Override
    protected void runImpl() throws Exception {
        var optionalInfo = TeleportListData.getInstance().getInfo(id);
        if (optionalInfo.isPresent()) {
            TeleportListInfo info = optionalInfo.get();

            var player = getClient().getPlayer();
            if ((player.getLevel() >= Config.ESSENCE_TELEPORT_FREE_LEVEL) && !player.destroyItemByItemId("Teleport", CommonItem.ADENA, info.getPrice(), null, true)) {
                player.sendPacket(SystemMessageId.YOU_DO_NOT_HAVE_ENOUGH_ADENA);
            } else if (!player.isAlikeDead()) {
                player.teleToLocation(info.getLocation());
            }
        }
    }
}
