package org.l2j.gameserver.network;

import io.github.joealisson.mmocore.PacketBuffer;
import org.l2j.gameserver.network.clientpackets.*;
import org.l2j.gameserver.network.clientpackets.friend.RequestAnswerFriendInvite;
import org.l2j.gameserver.network.clientpackets.friend.RequestFriendDel;
import org.l2j.gameserver.network.clientpackets.friend.RequestFriendInvite;
import org.l2j.gameserver.network.clientpackets.friend.RequestFriendList;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Supplier;

import static java.lang.Short.toUnsignedInt;
import static java.util.Objects.requireNonNullElse;

/**
 * @author UnAfraid
 */
public enum IncomingPackets implements PacketFactory {
    LOGOUT(Logout::new, ConnectionState.AUTHENTICATED, ConnectionState.IN_GAME), //UNetworkHandler::SendLogOutPacket
    ATTACK(Attack::new, ConnectionState.IN_GAME), //UNetworkHandler::Atk
    MOVE_BACKWARD_TO_LOCATION(MoveBackwardToLocation::new, ConnectionState.IN_GAME),
    START_PLEDGE_WAR(RequestStartPledgeWar::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestStartPledgeWar
    NOT_USE_10(null, ConnectionState.IN_GAME),
    STOP_PLEDGE_WAR(RequestStopPledgeWar::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestStopPledgeWar
    NOT_USE_11(null, ConnectionState.IN_GAME),
    NOT_USE_12(null, ConnectionState.IN_GAME),
    NOT_USE_13(null, ConnectionState.IN_GAME),
    SET_PLEDGE_CREST(RequestSetPledgeCrest::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestSetPledgeCrest
    NOT_USE_14(null, ConnectionState.IN_GAME),
    GIVE_NICKNAME(RequestGiveNickName::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestGiveNickName
    CHARACTER_CREATE(CharacterCreate::new, ConnectionState.AUTHENTICATED), //UNetworkHandler::RequestCharacterCreate
    CHARACTER_DELETE(CharacterDelete::new, ConnectionState.AUTHENTICATED), //UNetworkHandler::RequestCharacterDelete
    VERSION(ProtocolVersion::new, ConnectionState.CONNECTED), //CClientSocket::OnUserConnect
    MOVE_TO_LOCATION(MoveBackwardToLocation::new, ConnectionState.IN_GAME), //UNetworkHandler::MoveBackwardToLocation
    NOT_USE_34(null, ConnectionState.IN_GAME),
    ENTER_WORLD(EnterWorld::new, ConnectionState.JOINING_GAME), //UNetworkHandler::RequestEnterWorld
    CHARACTER_SELECT(CharacterSelect::new, ConnectionState.AUTHENTICATED), //UNetworkHandler::RequestGameStart
    NEW_CHARACTER(NewCharacter::new, ConnectionState.AUTHENTICATED), //UNetworkHandler::RequestNewCharacter
    ITEMLIST(RequestItemList::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestItemList
    NOT_USE_1(null, ConnectionState.IN_GAME),
    UNEQUIP_ITEM(RequestUnEquipItem::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestUnEquipItem
    DROP_ITEM(RequestDropItem::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestDropItem
    GET_ITEM(null, ConnectionState.IN_GAME),
    USE_ITEM(UseItem::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestUseItem
    TRADE_REQUEST(TradeRequest::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestTrade
    TRADE_ADD(AddTradeItem::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestAddTradeItem
    TRADE_DONE(TradeDone::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestTradeDone
    NOT_USE_35(null, ConnectionState.IN_GAME),
    NOT_USE_36(null, ConnectionState.IN_GAME),
    ACTION(Action::new, ConnectionState.IN_GAME), //UNetworkHandler::Action
    NOT_USE_37(null, ConnectionState.IN_GAME),
    NOT_USE_38(null, ConnectionState.IN_GAME),
    LINK_HTML(RequestLinkHtml::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestLinkHtml
    PASS_CMD_TO_SERVER(RequestBypassToServer::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestLinkHtml
    WRITE_BBS(RequestBBSwrite::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestBBSWrite
    NOT_USE_39(null, ConnectionState.IN_GAME),
    JOIN_PLEDGE(RequestJoinPledge::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestJoinPledge
    ANSWER_JOIN_PLEDGE(RequestAnswerJoinPledge::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestAnswerJoinPledge
    WITHDRAWAL_PLEDGE(RequestWithdrawalPledge::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestWithDrawalPledge
    OUST_PLEDGE_MEMBER(RequestOustPledgeMember::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestOustPledgeMember
    NOT_USE_40(null, ConnectionState.IN_GAME),
    LOGIN(AuthLogin::new, ConnectionState.CONNECTED), //ExQueueTicket
    GET_ITEM_FROM_PET(RequestGetItemFromPet::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestGetItemFromPet
    NOT_USE_22(null, ConnectionState.IN_GAME),
    ALLIANCE_INFO(RequestAllyInfo::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestAllyInfo
    CRYSTALLIZE_ITEM(RequestCrystallizeItem::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestCrystallizeItem
    NOT_USE_19(null, ConnectionState.IN_GAME),
    PRIVATE_STORE_LIST_SET(SetPrivateStoreListSell::new, ConnectionState.IN_GAME), //UNetworkHandler::SetPrivateStoreList
    PRIVATE_STORE_MANAGE_CANCEL(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestPrivateStoreManageCancel
    NOT_USE_41(null, ConnectionState.IN_GAME),
    SOCIAL_ACTION(null, ConnectionState.IN_GAME), //UNetworkHandler::SocialAction
    CHANGE_MOVE_TYPE(null, ConnectionState.IN_GAME), //UNetworkHandler::ChangeMoveType
    CHANGE_WAIT_TYPE(null, ConnectionState.IN_GAME), //UNetworkHandler::ChangeWaitType
    SELL_LIST(RequestSellItem::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestSellItem
    MAGIC_SKILL_LIST(RequestMagicSkillList::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestMagicSkillList
    MAGIC_SKILL_USE(RequestMagicSkillUse::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestMagicSkillUse
    APPEARING(Appearing::new, ConnectionState.IN_GAME), //UNetworkHandler::SAP
    WAREHOUSE_DEPOSIT_LIST(SendWareHouseDepositList::new, ConnectionState.IN_GAME), //UNetworkHandler::SendWareHouseDepositList
    WAREHOUSE_WITHDRAW_LIST(SendWareHouseWithDrawList::new, ConnectionState.IN_GAME), //UNetworkHandler::SendWareHouseWithdrawList
    SHORTCUT_REG(RequestShortCutReg::new, ConnectionState.IN_GAME),//UNetworkHandler::RequestShortCutReg
    NOT_USE_3(null, ConnectionState.IN_GAME),
    DEL_SHORTCUT(RequestShortCutDel::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestShortCutDel
    BUY_LIST(RequestBuyItem::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestBuyItem
    NOT_USE_2(null, ConnectionState.IN_GAME),
    JOIN_PARTY(RequestJoinParty::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestJoinParty
    ANSWER_JOIN_PARTY(RequestAnswerJoinParty::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestAnswerJoinParty
    WITHDRAWAL_PARTY(RequestWithDrawalParty::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestWithDrawalParty
    OUST_PARTY_MEMBER(RequestOustPartyMember::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestOustPartyMember
    DISMISS_PARTY(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestDismissParty
    CAN_NOT_MOVE_ANYMORE(CannotMoveAnymore::new, ConnectionState.IN_GAME),  //UNetworkHandler::CanNotMoveAnymore
    TARGET_UNSELECTED(RequestTargetCanceld::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestTargetCancel
    SAY2(Say2::new, ConnectionState.IN_GAME), //UNetworkHandler::Say2
    NOT_USE_42(null, ConnectionState.IN_GAME),
    NOT_USE_4(null, ConnectionState.IN_GAME),
    NOT_USE_5(null, ConnectionState.IN_GAME),
    PLEDGE_REQ_SHOW_MEMBER_LIST_OPEN(RequestPledgeMemberList::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestPledgeMemberList
    NOT_USE_6(null, ConnectionState.IN_GAME),
    NOT_USE_7(null, ConnectionState.IN_GAME),
    SKILL_LIST(RequestSkillList::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestSkillItem
    NOT_USE_8(null, ConnectionState.IN_GAME),
    MOVE_WITH_DELTA(MoveWithDelta::new, ConnectionState.IN_GAME), //UNetworkHandler::MoveWithDelta
    GETON_VEHICLE(RequestGetOnVehicle::new, ConnectionState.IN_GAME), //UNetworkHandler::GetOnVehicle
    GETOFF_VEHICLE(RequestGetOffVehicle::new, ConnectionState.IN_GAME), //UNetworkHandler::GetOffVehicle
    TRADE_START(AnswerTradeRequest::new, ConnectionState.IN_GAME), //UNetworkHandler::AnswerTradeRequest
    ICON_ACTION(RequestActionUse::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestActionUse
    RESTART(RequestRestart::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestRestart
    NOT_USE_9(null, ConnectionState.IN_GAME),
    VALIDATE_POSITION(ValidatePosition::new, ConnectionState.IN_GAME), //UNetworkHandler::ValidatePosition
    SEK_COSTUME(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestSEKCustom
    START_ROTATING(StartRotating::new, ConnectionState.IN_GAME), //UNetworkHandler::StartRotating
    FINISH_ROTATING(FinishRotating::new, ConnectionState.IN_GAME), //UNetworkHandler::FinishRotating
    NOT_USE_15(null, ConnectionState.IN_GAME),
    SHOW_BOARD(RequestShowBoard::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestShowBoard
    REQUEST_ENCHANT_ITEM(RequestEnchantItem::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestEnchantItem
    DESTROY_ITEM(RequestDestroyItem::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestDestroyItem
    TARGET_USER_FROM_MENU(null, ConnectionState.IN_GAME),
    QUESTLIST(RequestQuestList::new, ConnectionState.IN_GAME),
    DESTROY_QUEST(RequestQuestAbort::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestDestroyQuest
    NOT_USE_16(null, ConnectionState.IN_GAME),
    PLEDGE_INFO(RequestPledgeInfo::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestDestroyQuest
    PLEDGE_EXTENDED_INFO(RequestPledgeExtendedInfo::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestPledgeExtendedInfo
    PLEDGE_CREST(RequestPledgeCrest::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestPledgeCrest
    NOT_USE_17(null, ConnectionState.IN_GAME),
    NOT_USE_18(null, ConnectionState.IN_GAME),
    L2_FRIEND_LIST(null, ConnectionState.IN_GAME),  //UNetworkHandler::RequestFriendInfoList
    // discard this packet, the friend information is sent upon login no need to send all time since the information is keep on client
    L2_FRIEND_SAY(PacketFactory.DISCARD, ConnectionState.IN_GAME),   //UNetworkHandler::RequestSendL2FriendSay
    OPEN_MINIMAP(RequestShowMiniMap::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestOpenMinimap
    MSN_CHAT_LOG(null, ConnectionState.IN_GAME),  //UNetworkHandler::RequestSendMsnChatLog
    RELOAD(RequestRecordInfo::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestReload
    HENNA_EQUIP(RequestHennaEquip::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestHennaEquip
    HENNA_UNEQUIP_LIST(RequestHennaRemoveList::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestHennaUnequipList
    HENNA_UNEQUIP_INFO(RequestHennaItemRemoveInfo::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestHennaUnequipInfo
    HENNA_UNEQUIP(RequestHennaRemove::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestHennaUnequip
    ACQUIRE_SKILL_INFO(RequestAcquireSkillInfo::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestAcquireSkillInfo
    SYS_CMD_2(SendBypassBuildCmd::new, ConnectionState.IN_GAME),  //UNetworkHandler::SendBypassBuildCmd
    MOVE_TO_LOCATION_IN_VEHICLE(RequestMoveToLocationInVehicle::new, ConnectionState.IN_GAME),  //UNetworkHandler::MTL
    CAN_NOT_MOVE_ANYMORE_IN_VEHICLE(CannotMoveAnymoreInVehicle::new, ConnectionState.IN_GAME),  //UNetworkHandler::CanNotMoveAnymore
    FRIEND_ADD_REQUEST(RequestFriendInvite::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestFriendInvite
    FRIEND_ADD_REPLY(RequestAnswerFriendInvite::new, ConnectionState.IN_GAME),  //FriendAddRequest
    FRIEND_LIST(RequestFriendList::new, ConnectionState.IN_GAME),
    FRIEND_REMOVE(RequestFriendDel::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestFriendDel
    RESTORE_CHARACTER(CharacterRestore::new, ConnectionState.AUTHENTICATED),  //UNetworkHandler::RequestCharacterRestore
    REQ_ACQUIRE_SKILL(RequestAcquireSkill::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestAcquireSkill
    RESTART_POINT(RequestRestartPoint::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestRestartPoint
    GM_COMMAND_TYPE(RequestGMCommand::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestGMCommand
    LIST_PARTY_WAITING(RequestPartyMatchConfig::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestListPartyWating
    MANAGE_PARTY_ROOM(RequestPartyMatchList::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestManagePartyRoom
    JOIN_PARTY_ROOM(RequestPartyMatchDetail::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestJoinPartyRoom
    NOT_USE_20(null, ConnectionState.IN_GAME),
    PRIVATE_STORE_BUY_LIST_SEND(RequestPrivateStoreBuy::new, ConnectionState.IN_GAME),  //UNetworkHandler::SendPrivateStoreBuyList
    NOT_USE_21(null, ConnectionState.IN_GAME),
    TUTORIAL_LINK_HTML(RequestTutorialLinkHtml::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestTutorialLinkHtml
    TUTORIAL_PASS_CMD_TO_SERVER(RequestTutorialPassCmdToServer::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestTutorialPassCmdToServer
    TUTORIAL_MARK_PRESSED(RequestTutorialQuestionMark::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestTutorialMarkPressed
    TUTORIAL_CLIENT_EVENT(RequestTutorialClientEvent::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestTutorialClientEvent
    PETITION(RequestPetition::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestPetition
    PETITION_CANCEL(RequestPetitionCancel::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestPetitionCancel
    GMLIST(RequestGmList::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestGMList
    JOIN_ALLIANCE(RequestJoinAlly::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestJoinAlly
    ANSWER_JOIN_ALLIANCE(RequestAnswerJoinAlly::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestAnswerJoinAlly
    WITHDRAW_ALLIANCE(AllyLeave::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestWithdrawAlly
    OUST_ALLIANCE_MEMBER_PLEDGE(AllyDismiss::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestOustAlly
    DISMISS_ALLIANCE(RequestDismissAlly::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestDismissAlly
    SET_ALLIANCE_CREST(RequestSetAllyCrest::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestSetAllyCrest
    ALLIANCE_CREST(RequestAllyCrest::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestAllyCrest
    CHANGE_PET_NAME(RequestChangePetName::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestChangePetName
    PET_USE_ITEM(RequestPetUseItem::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestPetUseItem
    GIVE_ITEM_TO_PET(RequestGiveItemToPet::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestGiveItemToPet
    PRIVATE_STORE_QUIT(RequestPrivateStoreQuitSell::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestPrivateStoreQuit
    PRIVATE_STORE_SET_MSG(SetPrivateStoreMsgSell::new, ConnectionState.IN_GAME),  //UNetworkHandler::SetPrivateStoreMsg
    PET_GET_ITEM(RequestPetGetItem::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestPetGetItem
    NOT_USE_23(null, ConnectionState.IN_GAME),  //UNetworkHandler::
    PRIVATE_STORE_BUY_LIST_SET(SetPrivateStoreListBuy::new, ConnectionState.IN_GAME),  //UNetworkHandler::SetPrivateStoreBuyList
    PRIVATE_STORE_BUY_MANAGE_CANCEL(RequestPrivateStoreManageBuy::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestPrivateStoreBuyManageCancel
    PRIVATE_STORE_BUY_QUIT(RequestPrivateStoreQuitBuy::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestPrivateStoreBuyQuit
    PRIVATE_STORE_BUY_SET_MSG(SetPrivateStoreMsgBuy::new, ConnectionState.IN_GAME),  //UNetworkHandler::SetPrivateStoreBuyMsg
    NOT_USE_24(null, ConnectionState.IN_GAME),  //UNetworkHandler::
    PRIVATE_STORE_BUY_BUY_LIST_SEND(RequestPrivateStoreSell::new, ConnectionState.IN_GAME),  //UNetworkHandler::SendPrivateStoreBuyBuyList
    NOT_USE_25(null, ConnectionState.IN_GAME),  //UNetworkHandler::
    NOT_USE_26(null, ConnectionState.IN_GAME),  //UNetworkHandler::
    NOT_USE_27(null, ConnectionState.IN_GAME),  //UNetworkHandler::
    NOT_USE_28(null, ConnectionState.IN_GAME),  //UNetworkHandler::
    NOT_USE_29(null, ConnectionState.IN_GAME),  //UNetworkHandler::
    NOT_USE_30(null, ConnectionState.IN_GAME),  //UNetworkHandler::
    REQUEST_SKILL_COOL_TIME(null, ConnectionState.JOINING_GAME, ConnectionState.IN_GAME),  //UNetworkHandler::RequestSkillCoolTime
    REQUEST_PACKAGE_SENDABLE_ITEM_LIST(RequestPackageSendableItemList::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestPackageSendableItemList
    REQUEST_PACKAGE_SEND(RequestPackageSend::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestPackageSend
    BLOCK_PACKET(RequestBlock::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestBlock
    CASTLE_SIEGE_INFO(RequestSiegeInfo::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestCastleSiegeInfo
    CASTLE_SIEGE_ATTACKER_LIST(RequestSiegeAttackerList::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestCastleSiegeAttackerList
    CASTLE_SIEGE_DEFENDER_LIST(RequestSiegeDefenderList::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestCastleSiegeDefenderList
    JOIN_CASTLE_SIEGE(RequestJoinSiege::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestJoinCastleSiege
    CONFIRM_CASTLE_SIEGE_WAITING_LIST(RequestConfirmSiegeWaitingList::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestConfirmCastleSiegeWaitingList
    SET_CASTLE_SIEGE_TIME(RequestSetCastleSiegeTime::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestSetCastleSiegeTime
    MULTI_SELL_CHOOSE(MultiSellChoose::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestMultiSellChoose
    NET_PING(null, ConnectionState.IN_GAME),
    REMAIN_TIME(null, ConnectionState.IN_GAME),  //UNetworkHandler::RequestRemainTime
    USER_CMD_BYPASS(BypassUserCmd::new, ConnectionState.IN_GAME),  //UNetworkHandler::BypassUserCmd
    SNOOP_QUIT(SnoopQuit::new, ConnectionState.IN_GAME),  //UNetworkHandler::GMSnoopEnd
    RECIPE_BOOK_OPEN(RequestRecipeBookOpen::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestRecipeBookOpen
    RECIPE_ITEM_DELETE(RequestRecipeBookDestroy::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestRecipeItemDelete
    RECIPE_ITEM_MAKE_INFO(RequestRecipeItemMakeInfo::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestRecipeItemMakeInfo
    RECIPE_ITEM_MAKE_SELF(RequestRecipeItemMakeSelf::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestRecipeItemMakeSelf
    NOT_USE_31(null, ConnectionState.IN_GAME),
    RECIPE_SHOP_MESSAGE_SET(RequestRecipeShopMessageSet::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestRecipeShopMessageSet
    RECIPE_SHOP_LIST_SET(RequestRecipeShopListSet::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestRecipeShopListSet
    RECIPE_SHOP_MANAGE_QUIT(RequestRecipeShopManageQuit::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestRecipeShopManageQuit
    RECIPE_SHOP_MANAGE_CANCEL(null, ConnectionState.IN_GAME),  //UNetworkHandler::RequestRecipeShopManageCancel
    RECIPE_SHOP_MAKE_INFO(RequestRecipeShopMakeInfo::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestRecipeShopMakeInfo
    RECIPE_SHOP_MAKE_DO(RequestRecipeShopMakeItem::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestRecipeShopMakeDo
    RECIPE_SHOP_SELL_LIST(RequestRecipeShopManagePrev::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestRecipeShopSellList
    OBSERVER_END(ObserverReturn::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestObserverEndPacket
    VOTE_SOCIALITY(null, ConnectionState.IN_GAME),
    HENNA_ITEM_LIST(RequestHennaItemList::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestHennaItemList
    HENNA_ITEM_INFO(RequestHennaItemInfo::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestHennaItemInfo
    BUY_SEED(RequestBuySeed::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestBuySeed
    CONFIRM_DLG(DlgAnswer::new, ConnectionState.IN_GAME),  //UNetworkHandler::ConfirmDlg
    BUY_PREVIEW_LIST(RequestPreviewItem::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestPreviewItem
    SSQ_STATUS(null, ConnectionState.IN_GAME),
    PETITION_VOTE(RequestPetitionFeedback::new, ConnectionState.IN_GAME),  //UNetworkHandler::PetitionVote
    NOT_USE_33(null, ConnectionState.IN_GAME),
    GAMEGUARD_REPLY(GameGuardReply::new, ConnectionState.IN_GAME),  //UNetworkHandler::ReplyGameGuardQuery
    MANAGE_PLEDGE_POWER(RequestPledgePower::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestPledgePower
    MAKE_MACRO(RequestMakeMacro::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestMakeMacro
    DELETE_MACRO(RequestDeleteMacro::new, ConnectionState.IN_GAME),  //UNetworkHandler::RequestDeleteMacro
    NOT_USE_32(null, ConnectionState.IN_GAME),
    EX_PACKET(null, true, ConnectionState.values()); // This packet has its own connection state checking so we allow all of them

    public static final IncomingPackets[] PACKET_ARRAY;

    static {
        final short maxPacketId = (short) Arrays.stream(values()).mapToInt(IncomingPackets::getPacketId).max().orElse(0);
        PACKET_ARRAY = new IncomingPackets[maxPacketId + 1];
        for (IncomingPackets incomingPacket : values()) {
            PACKET_ARRAY[incomingPacket.getPacketId()] = incomingPacket;
        }
    }

    private Supplier<ClientPacket> _incomingPacketFactory;
    private Set<ConnectionState> _connectionStates;
    private boolean hasExtension;

    IncomingPackets(Supplier<ClientPacket> incomingPacketFactory, boolean hasExtension, ConnectionState... connectionStates) {
        _incomingPacketFactory = incomingPacketFactory != null ? incomingPacketFactory : NULL_PACKET_SUPLIER;
        _connectionStates = new HashSet<>(Arrays.asList(connectionStates));
        this.hasExtension = hasExtension;
    }

    IncomingPackets(Supplier<ClientPacket> incomingPacketFactory, ConnectionState... connectionStates) {
        this(incomingPacketFactory, false, connectionStates);
    }

    @Override
    public int getPacketId() {
        return ordinal();
    }

    @Override
    public ClientPacket newIncomingPacket() {
        return _incomingPacketFactory.get();
    }

    @Override
    public Set<ConnectionState> getConnectionStates() {
        return _connectionStates;
    }

    @Override
    public boolean canHandleState(ConnectionState connectionState) {
        return _connectionStates.contains(connectionState);
    }

    @Override
    public boolean hasExtension() {
        return hasExtension;
    }

    @Override
    public PacketFactory handleExtension(PacketBuffer buffer) {
        var exPacketId = toUnsignedInt(buffer.readShort());
        if (exPacketId >= ExIncomingPackets.PACKET_ARRAY.length) {
            return NULLABLE_PACKET_FACTORY;
        }

        return requireNonNullElse(ExIncomingPackets.PACKET_ARRAY[exPacketId], NULLABLE_PACKET_FACTORY);
    }

}
