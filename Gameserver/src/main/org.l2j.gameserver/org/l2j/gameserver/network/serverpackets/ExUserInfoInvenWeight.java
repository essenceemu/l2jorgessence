package org.l2j.gameserver.network.serverpackets;

import org.l2j.gameserver.model.actor.instance.Player;
import org.l2j.gameserver.network.GameClient;
import org.l2j.gameserver.network.ServerPacketIdEx;

/**
 * @author Sdw
 */
public class ExUserInfoInvenWeight extends ServerPacket {
    private final Player _activeChar;

    public ExUserInfoInvenWeight(Player cha) {
        _activeChar = cha;
    }

    @Override
    public void writeImpl(GameClient client) {
        writeId(ServerPacketIdEx.EX_USER_INFO_INVENWEIGHT);

        writeInt(_activeChar.getObjectId());
        writeInt(_activeChar.getCurrentLoad());
        writeInt(_activeChar.getMaxLoad());
    }

}