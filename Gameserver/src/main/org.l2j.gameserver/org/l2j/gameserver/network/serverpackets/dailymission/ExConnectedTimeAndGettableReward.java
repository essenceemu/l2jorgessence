package org.l2j.gameserver.network.serverpackets.dailymission;

import org.l2j.gameserver.data.xml.impl.DailyMissionData;
import org.l2j.gameserver.model.actor.instance.Player;
import org.l2j.gameserver.network.GameClient;
import org.l2j.gameserver.network.ServerPacketIdEx;
import org.l2j.gameserver.network.serverpackets.ServerPacket;

/**
 * @author Sdw
 */
public class ExConnectedTimeAndGettableReward extends ServerPacket {
    private final int oneDayRewardAvailableCount;

    public ExConnectedTimeAndGettableReward(Player player) {
        oneDayRewardAvailableCount = DailyMissionData.getInstance().getAvailableDailyMissionCount(player);
    }

    public ExConnectedTimeAndGettableReward(int availableCount) {
        oneDayRewardAvailableCount = availableCount;
    }

    @Override
    public void writeImpl(GameClient client) {
        if (!DailyMissionData.getInstance().isAvailable()) {
            return;
        }

        writeId(ServerPacketIdEx.EX_ONE_DAY_REWARD_INFO);
        writeInt(0x00);
        writeInt(oneDayRewardAvailableCount);
        writeInt(0x00);
        writeInt(0x00);
        writeInt(0x00);
        writeInt(0x00);
        writeInt(0x00);
        writeInt(0x00);
        writeInt(0x00);
        writeInt(0x00);
        writeInt(0x00);
        writeInt(0x00);
    }

}
