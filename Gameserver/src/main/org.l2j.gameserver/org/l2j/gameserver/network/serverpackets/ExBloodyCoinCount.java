package org.l2j.gameserver.network.serverpackets;

import org.l2j.gameserver.network.GameClient;
import org.l2j.gameserver.network.ServerPacketIdEx;

public class ExBloodyCoinCount extends ServerPacket {
    @Override
    protected void writeImpl(GameClient client) throws Exception {
        writeId(ServerPacketIdEx.EX_BLOODY_COIN_COUNT);
        writeLong(client.getPlayer().getEssenceCoins());
    }
}
