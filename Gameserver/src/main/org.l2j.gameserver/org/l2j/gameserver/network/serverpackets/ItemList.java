package org.l2j.gameserver.network.serverpackets;

import org.l2j.gameserver.enums.ItemPackageType;
import org.l2j.gameserver.model.actor.instance.Player;
import org.l2j.gameserver.model.items.instance.Item;
import org.l2j.gameserver.network.GameClient;
import org.l2j.gameserver.network.ServerPacketId;

import java.util.Collection;

public final class ItemList extends AbstractItemPacket {
    private final ItemPackageType packageType;
    private final Player player;
    private final Collection<Item> items;

    public ItemList(ItemPackageType packageType, Player player) {
        this.packageType = packageType;
        this.player = player;
        items = player.getInventory().getItems(item -> !item.isQuestItem());
    }

    @Override
    public void writeImpl(GameClient client) {
        writeId(ServerPacketId.ITEMLIST);

        switch (packageType) {
            case BasicInfo -> {
                writeByte((byte) packageType.ordinal());
                writeShort(0x01); // _showWindow ? 0x01 : 0x00
                writeInventoryBlock(player.getInventory());
                writeInt(items.size());
            }
            case PagedItemInfo -> {
                writeByte((byte) packageType.ordinal());
                writeInt(items.size());
                writeInt(items.size());
                for (Item item : items) {
                    writeItem(item);
                }
            }
        }
    }
}
