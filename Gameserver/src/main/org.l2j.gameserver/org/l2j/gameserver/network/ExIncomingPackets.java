package org.l2j.gameserver.network;

import io.github.joealisson.mmocore.PacketBuffer;
import org.l2j.gameserver.network.clientpackets.*;
import org.l2j.gameserver.network.clientpackets.adenadistribution.RequestDivideAdena;
import org.l2j.gameserver.network.clientpackets.adenadistribution.RequestDivideAdenaCancel;
import org.l2j.gameserver.network.clientpackets.adenadistribution.RequestDivideAdenaStart;
import org.l2j.gameserver.network.clientpackets.attendance.RequestVipAttendanceCheck;
import org.l2j.gameserver.network.clientpackets.attendance.RequestVipAttendanceItemList;
import org.l2j.gameserver.network.clientpackets.attributechange.RequestChangeAttributeCancel;
import org.l2j.gameserver.network.clientpackets.attributechange.RequestChangeAttributeItem;
import org.l2j.gameserver.network.clientpackets.attributechange.SendChangeAttributeTargetItem;
import org.l2j.gameserver.network.clientpackets.ceremonyofchaos.RequestCuriousHouseHtml;
import org.l2j.gameserver.network.clientpackets.commission.*;
import org.l2j.gameserver.network.clientpackets.compound.*;
import org.l2j.gameserver.network.clientpackets.crystalization.RequestCrystallizeEstimate;
import org.l2j.gameserver.network.clientpackets.crystalization.RequestCrystallizeItemCancel;
import org.l2j.gameserver.network.clientpackets.dailymission.RequestOneDayRewardReceive;
import org.l2j.gameserver.network.clientpackets.dailymission.RequestTodoList;
import org.l2j.gameserver.network.clientpackets.ensoul.RequestItemEnsoul;
import org.l2j.gameserver.network.clientpackets.ensoul.RequestTryEnSoulExtraction;
import org.l2j.gameserver.network.clientpackets.friend.RequestFriendDetailInfo;
import org.l2j.gameserver.network.clientpackets.luckygame.RequestLuckyGamePlay;
import org.l2j.gameserver.network.clientpackets.pledgebonus.RequestPledgeBonusOpen;
import org.l2j.gameserver.network.clientpackets.pledgebonus.RequestPledgeBonusReward;
import org.l2j.gameserver.network.clientpackets.pledgebonus.RequestPledgeBonusRewardList;
import org.l2j.gameserver.network.clientpackets.primeshop.*;
import org.l2j.gameserver.network.clientpackets.raidbossinfo.RequestRaidBossSpawnInfo;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Supplier;

/**
 * @author Sdw
 */
public enum ExIncomingPackets implements PacketFactory {
    EX_DUMMY(null, ConnectionState.IN_GAME),
    EX_REQ_MANOR_LIST(RequestManorList::new, ConnectionState.JOINING_GAME), //UNetworkHandler::RequestManorList
    EX_PROCURE_CROP_LIST(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestProcureCropList
    EX_SET_SEED(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestSetSeed
    EX_SET_CROP(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestSetCrop
    EX_WRITE_HERO_WORDS(RequestWriteHeroWords::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestWriteHeroWords
    EX_ASK_JOIN_MPCC(RequestExAskJoinMPCC::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestExAskJoinMPCC
    EX_ACCEPT_JOIN_MPCC(RequestExAcceptJoinMPCC::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestExAcceptJoinMPCC
    EX_OUST_FROM_MPCC(RequestExOustFromMPCC::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestExOustFromMPCC
    EX_OUST_FROM_PARTY_ROOM(RequestOustFromPartyRoom::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestOustFromPartyRoom
    EX_DISMISS_PARTY_ROOM(RequestDismissPartyRoom::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestDismissPartyRoom
    EX_WITHDRAW_PARTY_ROOM(RequestWithdrawPartyRoom::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestWithdrawPartyRoom
    EX_HAND_OVER_PARTY_MASTER(RequestChangePartyLeader::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestHandOverPartyMaster
    EX_AUTO_SOULSHOT(RequestAutoSoulShot::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestAutoSoulShot
    EX_ENCHANT_SKILL_INFO(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestExEnchantSkillInfo
    EX_REQ_ENCHANT_SKILL(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestExEnchantSkill
    EX_PLEDGE_EMBLEM(RequestExPledgeCrestLarge::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestExPledgeEmblem
    EX_SET_PLEDGE_EMBLEM(RequestExSetPledgeCrestLarge::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestExSetPledgeEmblem
    EX_SET_ACADEMY_MASTER(RequestPledgeSetAcademyMaster::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestPledgeSetAcademyMasterPupil
    EX_PLEDGE_POWER_GRADE_LIST(RequestPledgePowerGradeList::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestPledgePowerGradeList
    EX_VIEW_PLEDGE_POWER(RequestPledgeMemberPowerInfo::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestPledgeMemberPowerInfo
    EX_SET_PLEDGE_POWER_GRADE(RequestPledgeSetMemberPowerGrade::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestPledgeSetMemberPowerGrade
    EX_VIEW_PLEDGE_MEMBER_INFO(RequestPledgeMemberInfo::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestPledgeMemberInfo
    EX_VIEW_PLEDGE_WARLIST(RequestPledgeWarList::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestPledgeWarList
    EX_FISH_RANKING(null, ConnectionState.IN_GAME),
    EX_PCCAFE_COUPON_USE(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestPCCafeCouponUse
    EX_ORC_MOVE(null, ConnectionState.IN_GAME), //UNetworkHandler::ExSearchOrc
    EX_DUEL_ASK_START(RequestDuelStart::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestDuelStart
    EX_DUEL_ACCEPT_START(RequestDuelAnswerStart::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestDuelAnswerStart
    EX_SET_TUTORIAL(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestExSetTutorial
    EX_RQ_ITEMLINK(RequestExRqItemLink::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestExRqItemLink
    EX_CAN_NOT_MOVE_ANYMORE_IN_AIRSHIP(null, ConnectionState.IN_GAME), //UNetworkHandler::CanNotMoveAnymore
    EX_MOVE_TO_LOCATION_IN_AIRSHIP(null, ConnectionState.IN_GAME), //UNetworkHandler::MTL
    EX_LOAD_UI_SETTING(RequestKeyMapping::new, ConnectionState.JOINING_GAME), //UNetworkHandler::RequestKeyMapping
    EX_SAVE_UI_SETTING(RequestSaveKeyMapping::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestSaveKeyMapping
    EX_REQUEST_BASE_ATTRIBUTE_CANCEL(RequestExRemoveItemAttribute::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestExRemoveItemAttribute
    EX_CHANGE_INVENTORY_SLOT(RequestSaveInventoryOrder::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestSaveInventoryOrder
    EX_EXIT_PARTY_MATCHING_WAITING_ROOM(RequestExitPartyMatchingWaitingRoom::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestExitPartyMatchingWaitingRoom
    EX_TRY_TO_PUT_ITEM_FOR_VARIATION_MAKE(RequestConfirmTargetItem::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestConfirmTargetItem
    EX_TRY_TO_PUT_INTENSIVE_FOR_VARIATION_MAKE(RequestConfirmRefinerItem::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestConfirmRefinerItem
    EX_TRY_TO_PUT_COMMISSION_FOR_VARIATION_MAKE(RequestConfirmGemStone::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestConfirmGemStone
    EX_OLYMPIAD_OBSERVER_END(RequestOlympiadObserverEnd::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestOlympiadObserverEnd
    EX_CURSED_WEAPON_LIST(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestCursedWeaponList
    EX_EXISTING_CURSED_WEAPON_LOCATION(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestCursedWeaponLocation
    EX_REORGANIZE_PLEDGE_MEMBER(RequestPledgeReorganizeMember::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestPledgeReorganizeMember
    EX_MPCC_SHOW_PARTY_MEMBERS_INFO(RequestExMPCCShowPartyMembersInfo::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestExMPCCShowPartyMembersInfo
    EX_OLYMPIAD_MATCH_LIST(null, ConnectionState.IN_GAME),
    EX_ASK_JOIN_PARTY_ROOM(RequestAskJoinPartyRoom::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestAskJoinPartyRoom
    EX_ANSWER_JOIN_PARTY_ROOM(AnswerJoinPartyRoom::new, ConnectionState.IN_GAME), //UNetworkHandler::AnswerJoinPartyRoom
    EX_LIST_PARTY_MATCHING_WAITING_ROOM(RequestListPartyMatchingWaitingRoom::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestPartyMatchWaitList
    EX_CHOOSE_INVENTORY_ATTRIBUTE_ITEM(RequestExEnchantItemAttribute::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestExEnchantItemAttribute
    EX_CHARACTER_BACK(RequestGotoLobby::new, ConnectionState.AUTHENTICATED), //UNetworkHandler::RequestGotoLobby
    EX_CANNOT_AIRSHIP_MOVE_ANYMORE(null, ConnectionState.IN_GAME),
    EX_MOVE_TO_LOCATION_AIRSHIP(null, ConnectionState.IN_GAME), //UNetworkHandler::MTL
    EX_ITEM_AUCTION_BID(RequestBidItemAuction::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestBidItemAuction
    EX_ITEM_AUCTION_INFO(RequestInfoItemAuction::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestInfoItemAuction
    EX_CHANGE_NAME(RequestExChangeName::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestExChangeName
    EX_SHOW_CASTLE_INFO(RequestAllCastleInfo::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestAllCastleInfo
    EX_SHOW_FORTRESS_INFO(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestAllFortressInfo
    EX_SHOW_AGIT_INFO(RequestAllAgitInfo::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestAllAgitInfo
    EX_SHOW_FORTRESS_SIEGE_INFO(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestFortressSiegeInfo
    EX_GET_BOSS_RECORD(RequestGetBossRecord::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestGetBossRecord
    EX_TRY_TO_MAKE_VARIATION(RequestRefine::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestRefine
    EX_TRY_TO_PUT_ITEM_FOR_VARIATION_CANCEL(RequestConfirmCancelItem::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestConfirmCancelItem
    EX_CLICK_VARIATION_CANCEL_BUTTON(RequestRefineCancel::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestRefineCancel
    EX_MAGIC_SKILL_USE_GROUND(RequestExMagicSkillUseGround::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestExMagicSkillUseGround
    EX_DUEL_SURRENDER(RequestDuelSurrender::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestDuelSurrender
    EX_ENCHANT_SKILL_INFO_DETAIL(RequestExEnchantSkillInfoDetail::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestExEnchantSkillInfoDetail
    EX_REQUEST_ANTI_FREE_SERVER(null, ConnectionState.IN_GAME),
    EX_SHOW_FORTRESS_MAP_INFO(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestFortressMapInfo
    EX_REQUEST_PVPMATCH_RECORD(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestPVPMatchRecord
    EX_PRIVATE_STORE_WHOLE_SET_MSG(SetPrivateStoreWholeMsg::new, ConnectionState.IN_GAME), //UNetworkHandler::SetPrivateStoreWholeMsg
    EX_DISPEL(RequestDispel::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestDispel
    EX_TRY_TO_PUT_ENCHANT_TARGET_ITEM(RequestExTryToPutEnchantTargetItem::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestExTryToPutEnchantTargetItem
    EX_TRY_TO_PUT_ENCHANT_SUPPORT_ITEM(RequestExTryToPutEnchantSupportItem::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestExTryToPutEnchantSupportItem
    EX_CANCEL_ENCHANT_ITEM(RequestExCancelEnchantItem::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestExCancelEnchantItem
    EX_CHANGE_NICKNAME_COLOR(RequestChangeNicknameColor::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestChangeNicknameColor
    EX_REQUEST_RESET_NICKNAME(RequestResetNickname::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestResetNickname
    EX_USER_BOOKMARK(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestChangeBookMarkSlot
    EX_WITHDRAW_PREMIUM_ITEM(RequestWithDrawPremiumItem::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestWithDrawPremiumItem
    EX_JUMP(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestJump
    EX_START_REQUEST_PVPMATCH_CC_RANK(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestStartShowCrataeCubeRank
    EX_STOP_REQUEST_PVPMATCH_CC_RANK(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestStopShowCrataeCubeRank
    EX_NOTIFY_START_MINIGAME(null, ConnectionState.IN_GAME), //UNetworkHandler::NotifyStartMiniGame
    EX_REQUEST_REGISTER_DOMINION(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestJoinDominionWar
    EX_REQUEST_DOMINION_INFO(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestDominionInfo
    EX_CLEFT_ENTER(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestExCleftEnter
    EX_BLOCK_UPSET_ENTER(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestExBlockGameEnter
    EX_END_SCENE_PLAYER(EndScenePlayer::new, ConnectionState.IN_GAME), //UNetworkHandler::EndScenePlayer
    EX_BLOCK_UPSET_VOTE(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestExBlockGameVote
    EX_LIST_MPCC_WAITING(RequestExListMpccWaiting::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestListMpccWaiting
    EX_MANAGE_MPCC_ROOM(RequestExManageMpccRoom::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestManageMpccRoom
    EX_JOIN_MPCC_ROOM(RequestExJoinMpccRoom::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestJoinMpccRoom
    EX_OUST_FROM_MPCC_ROOM(RequestExOustFromMpccRoom::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestOustFromMpccRoom
    EX_DISMISS_MPCC_ROOM(RequestExDismissMpccRoom::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestDismissMpccRoom
    EX_WITHDRAW_MPCC_ROOM(RequestExWithdrawMpccRoom::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestWithdrawMpccRoom
    EX_SEED_PHASE(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestSeedPhase
    EX_MPCC_PARTYMASTER_LIST(RequestExMpccPartymasterList::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestMpccPartymasterList
    EX_REQUEST_POST_ITEM_LIST(RequestPostItemList::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestPostItemList
    EX_SEND_POST(RequestSendPost::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestSendPost
    EX_REQUEST_RECEIVED_POST_LIST(RequestReceivedPostList::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestRequestReceivedPostList
    EX_DELETE_RECEIVED_POST(RequestDeleteReceivedPost::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestDeleteReceivedPost
    EX_REQUEST_RECEIVED_POST(RequestReceivedPost::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestRequestReceivedPost
    EX_RECEIVE_POST(RequestPostAttachment::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestReceivePost
    EX_REJECT_POST(RequestRejectPostAttachment::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestRejectPost
    EX_REQUEST_SENT_POST_LIST(RequestSentPostList::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestRequestSentPostList
    EX_DELETE_SENT_POST(RequestDeleteSentPost::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestDeleteSentPost
    EX_REQUEST_SENT_POST(RequestSentPost::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestRequestSentPost
    EX_CANCEL_SEND_POST(RequestCancelPostAttachment::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestCancelSentPost
    EX_REQUEST_SHOW_PETITION(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestShowNewUserPetition
    EX_REQUEST_SHOWSTEP_TWO(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestShowStepTwo
    EX_REQUEST_SHOWSTEP_THREE(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestShowStepThree
    EX_CONNECT_TO_RAID_SERVER(null, ConnectionState.IN_GAME), //ExRaidReserveResult
    EX_RETURN_FROM_RAID(null, ConnectionState.IN_GAME), //ExCloseRaidSocket
    EX_REFUND_REQ(RequestRefundItem::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestRefundItem
    EX_BUY_SELL_UI_CLOSE_REQ(RequestBuySellUIClose::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestBuySellUIClose
    EX_EVENT_MATCH(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestEventMatchObserverEnd
    EX_PARTY_LOOTING_MODIFY(RequestPartyLootModification::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestPartyLootingModify
    EX_PARTY_LOOTING_MODIFY_AGREEMENT(AnswerPartyLootModification::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestPartyLootingModifyAgreement
    EX_ANSWER_COUPLE_ACTION(AnswerCoupleAction::new, ConnectionState.IN_GAME), //UNetworkHandler::AnswerCoupleAction
    EX_BR_LOAD_EVENT_TOP_RANKERS_REQ(BrEventRankerList::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestBR_EventRankerList
    EX_ASK_MY_MEMBERSHIP(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestAskMemberShip
    EX_QUEST_NPC_LOG_LIST(RequestAddExpandQuestAlarm::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestAddExpandQuestAlarm
    EX_VOTE_SYSTEM(RequestVoteNew::new, ConnectionState.IN_GAME), //UNetworkHandler::NewVoteSociality
    EX_GETON_SHUTTLE(null, ConnectionState.IN_GAME), //UNetworkHandler::GetOnShuttle
    EX_GETOFF_SHUTTLE(null, ConnectionState.IN_GAME), //UNetworkHandler::GetOffShuttle
    EX_MOVE_TO_LOCATION_IN_SHUTTLE(null, ConnectionState.IN_GAME), //UNetworkHandler::MTL
    EX_CAN_NOT_MOVE_ANYMORE_IN_SHUTTLE(null, ConnectionState.IN_GAME), //UNetworkHandler::CanNotMoveAnymore
    EX_AGITAUCTION_CMD(null, ConnectionState.IN_GAME), // TODO: Implement / HANDLE SWITCH
    EX_ADD_POST_FRIEND(RequestExAddContactToContactList::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestExAddPostFriendForPostBox
    EX_DELETE_POST_FRIEND(RequestExDeleteContactFromContactList::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestExDeletePostFriendForPostBox
    EX_SHOW_POST_FRIEND(RequestExShowContactList::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestExShowPostFriendListForPostBox
    EX_FRIEND_LIST_FOR_POSTBOX(null, ConnectionState.IN_GAME),
    EX_GFX_OLYMPIAD(RequestExOlympiadMatchListRefresh::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestOlympiadMatchList
    EX_BR_GAME_POINT_REQ(RequestBRGamePoint::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestBR_GamePoint
    EX_BR_PRODUCT_LIST_REQ(RequestBRProductList::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestBR_ProductList
    EX_BR_PRODUCT_INFO_REQ(RequestBRProductInfo::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestBR_ProductInfo
    EX_BR_BUY_PRODUCT_REQ(RequestBRBuyProduct::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestBR_BuyProduct
    EX_BR_RECENT_PRODUCT_REQ(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestBR_RecentProductList
    EX_BR_MINIGAME_LOAD_SCORES_REQ(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestBR_MinigameLoadScores
    EX_BR_MINIGAME_INSERT_SCORE_REQ(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestBR_MinigameInsertScore
    EX_BR_SET_LECTURE_MARK_REQ(null, ConnectionState.IN_GAME), //UNetworkHandler::RequstBR_LectureMark
    EX_REQUEST_CRYSTALITEM_INFO(RequestCrystallizeEstimate::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestCrystallizeEstimate
    EX_REQUEST_CRYSTALITEM_CANCEL(RequestCrystallizeItemCancel::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestCrystallizeItemCancel
    EX_STOP_SCENE_PLAYER(RequestExEscapeScene::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestExEscapeScene
    EX_FLY_MOVE(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestFlyMove
    EX_SURRENDER_PLEDGE_WAR(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestSurrenderPledgeWarEX
    EX_DYNAMIC_QUEST(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestDynamicQuest, TODO: Implement / HANDLE SWITCH
    EX_FRIEND_DETAIL_INFO(RequestFriendDetailInfo::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestFriendDetailInfo
    EX_UPDATE_FRIEND_MEMO(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestUpdateFriendMemo
    EX_UPDATE_BLOCK_MEMO(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestUpdateBlockMemo
    EX_LOAD_INZONE_PARTY_HISTORY(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestInzonePartyInfoHistory
    EX_REQUEST_COMMISSION_ITEM_LIST(RequestCommissionRegistrableItemList::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestCommissionRegistrableItemList
    EX_REQUEST_COMMISSION_INFO(RequestCommissionInfo::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestCommissionInfo
    EX_REQUEST_COMMISSION_REGISTER(RequestCommissionRegister::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestCommissionRegister
    EX_REQUEST_COMMISSION_CANCEL(RequestCommissionCancel::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestCommissionCancel
    EX_REQUEST_COMMISSION_DELETE(RequestCommissionDelete::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestCommissionDelete
    EX_REQUEST_COMMISSION_SEARCH(RequestCommissionList::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestCommissionList
    EX_REQUEST_COMMISSION_BUY_INFO(RequestCommissionBuyInfo::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestCommissionBuyInfo
    EX_REQUEST_COMMISSION_BUY_ITEM(RequestCommissionBuyItem::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestCommissionBuyItem
    EX_REQUEST_COMMISSION_REGISTERED_ITEM(RequestCommissionRegisteredItem::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestCommissionRegisteredItem
    EX_CALL_TO_CHANGE_CLASS(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestCallToChangeClass
    EX_CHANGE_TO_AWAKENED_CLASS(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestChangeToAwakenedClass
    EX_NOT_USED_163(null, ConnectionState.IN_GAME), //UNetworkHandler::
    EX_NOT_USED_164(null, ConnectionState.IN_GAME), //UNetworkHandler::
    EX_REQUEST_WEB_SESSION_ID(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestWebSessionID
    EX_2ND_PASSWORD_CHECK(RequestEx2ndPasswordCheck::new, ConnectionState.AUTHENTICATED), //UNetworkHandler::RequestEx2ndPasswordCheck
    EX_2ND_PASSWORD_VERIFY(RequestEx2ndPasswordVerify::new, ConnectionState.AUTHENTICATED), //UNetworkHandler::RequestEx2ndPasswordVerify
    EX_2ND_PASSWORD_REQ(RequestEx2ndPasswordReq::new, ConnectionState.AUTHENTICATED), //UNetworkHandler::RequestEx2ndPasswordReq
    EX_CHECK_CHAR_NAME(RequestCharacterNameCreatable::new, ConnectionState.AUTHENTICATED), //UNetworkHandler::RequestCharacterNameCreatable
    EX_REQUEST_GOODS_INVENTORY_INFO(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestGoodsInventoryInfo
    EX_REQUEST_USE_GOODS_IVENTORY_ITEM(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestUseGoodsInventoryItem
    EX_NOTIFY_PLAY_START(null, ConnectionState.IN_GAME),
    EX_FLY_MOVE_START(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestFlyMoveStart
    EX_USER_HARDWARE_INFO(RequestHardWareInfo::new, ConnectionState.values()), //UNetworkHandler::RequestHardWareInfo
    EX_USER_INTERFACE_INFO(null, ConnectionState.values()),
    EX_CHANGE_ATTRIBUTE_ITEM(SendChangeAttributeTargetItem::new, ConnectionState.IN_GAME), //UNetworkHandler::SendChangeAttributeTargetItem
    EX_REQUEST_CHANGE_ATTRIBUTE(RequestChangeAttributeItem::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestChangeAttributeItem
    EX_CHANGE_ATTRIBUTE_CANCEL(RequestChangeAttributeCancel::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestChangeAttributeCancel
    EX_BR_BUY_PRODUCT_GIFT_REQ(RequestBRPresentBuyProduct::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestBR_PresentBuyProduct
    EX_MENTOR_ADD(null, ConnectionState.IN_GAME), //UNetworkHandler::ConfirmMenteeAdd
    EX_MENTOR_CANCEL(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestMentorCancel
    EX_MENTOR_LIST(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestMentorList
    EX_REQUEST_MENTOR_ADD(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestMenteeAdd
    EX_MENTEE_WAITING_LIST(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestMenteeWaitingList
    EX_JOIN_PLEDGE_BY_NAME(RequestClanAskJoinByName::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestJoinPledgeByName
    EX_INZONE_WAITING_TIME(RequestInzoneWaitingTime::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestInzoneWaitingTime
    EX_JOIN_CURIOUS_HOUSE(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestJoinCuriousHouse
    EX_CANCEL_CURIOUS_HOUSE(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestCancelCuriousHouse
    EX_LEAVE_CURIOUS_HOUSE(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestLeaveCuriousHouse
    EX_OBSERVE_LIST_CURIOUS_HOUSE(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestObservingListCuriousHouse
    EX_OBSERVE_CURIOUS_HOUSE(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestObservingCuriousHouse
    EX_EXIT_OBSERVE_CURIOUS_HOUSE(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestLeaveObservingCuriousHouse
    EX_REQ_CURIOUS_HOUSE_HTML(RequestCuriousHouseHtml::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestCuriousHouseHtml
    EX_REQ_CURIOUS_HOUSE_RECORD(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestCuriousHouseRecord
    EX_SYS_STRING(null, ConnectionState.IN_GAME), //ExSysstring
    EX_TRY_TO_PUT_SHAPE_SHIFTING_TARGET_ITEM(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestExTryToPut_Shape_Shifting_TargetItem
    EX_TRY_TO_PUT_SHAPE_SHIFTING_EXTRACTION_ITEM(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestExTryToPut_Shape_Shifting_EnchantSupportItem
    EX_CANCEL_SHAPE_SHIFTING(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestExCancelShape_Shifting_Item
    EX_REQUEST_SHAPE_SHIFTING(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestShape_Shifting_Item
    EX_NCGUARD(DISCARD, ConnectionState.IN_GAME), //UNetworkHandler::NCGuardSendDataToServer
    EX_REQUEST_KALIE_TOKEN(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestEventKalieToken
    EX_REQUEST_SHOW_REGIST_BEAUTY(RequestShowBeautyList::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestShowBeautyList
    EX_REQUEST_REGIST_BEAUTY(RequestRegistBeauty::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestRegistBeauty
    EX_REQUEST_RESET_BEAUTY(null, ConnectionState.IN_GAME),
    EX_REQUEST_SHOW_RESET_BEAUTY(RequestShowResetShopList::new, ConnectionState.IN_GAME), //UNetworkHandler::
    EX_CHECK_SPEEDHACK(null, ConnectionState.IN_GAME),
    EX_BR_ADD_INTERESTED_PRODUCT(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestBR_AddBasketProductInfo
    EX_BR_DELETE_INTERESTED_PRODUCT(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestBR_DeleteBasketProductInfo
    EX_BR_EXIST_NEW_PRODUCT_REQ(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestBR_NewIConCashBtnWnd
    EX_EVENT_CAMPAIGN_INFO(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestEventCampaign... TODO: Implement / HANDLE SWITCH
    EX_PLEDGE_RECRUIT_INFO(RequestPledgeRecruitInfo::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestPledgeRecruitInfo
    EX_PLEDGE_RECRUIT_BOARD_SEARCH(RequestPledgeRecruitBoardSearch::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestPledgeRecruitBoardSearch
    EX_PLEDGE_RECRUIT_BOARD_APPLY(RequestPledgeRecruitBoardAccess::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestPledgeRecruitBoardAccess
    EX_PLEDGE_RECRUIT_BOARD_DETAIL(RequestPledgeRecruitBoardDetail::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestPledgeRecruitBoardDetail
    EX_PLEDGE_WAITING_LIST_APPLY(RequestPledgeWaitingApply::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestPledgeWaitingApply
    EX_PLEDGE_WAITING_LIST_APPLIED(RequestPledgeWaitingApplied::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestPledgeWaitingApplied
    EX_PLEDGE_WAITING_LIST(RequestPledgeWaitingList::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestPledgeWaitingList
    EX_PLEDGE_WAITING_USER(RequestPledgeWaitingUser::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestPledgeWaitingUser
    EX_PLEDGE_WAITING_USER_ACCEPT(RequestPledgeWaitingUserAccept::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestPledgeWaitingUserAccept
    EX_PLEDGE_DRAFT_LIST_SEARCH(RequestPledgeDraftListSearch::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestPledgeDraftListSearch
    EX_PLEDGE_DRAFT_LIST_APPLY(RequestPledgeDraftListApply::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestPledgeDraftListApply
    EX_PLEDGE_RECRUIT_APPLY_INFO(RequestPledgeRecruitApplyInfo::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestPledgeRecruitApplyInfo
    EX_PLEDGE_JOIN_SYS(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestPledgeJoinSys
    EX_RESPONSE_WEB_PETITION_ALARM(null, ConnectionState.IN_GAME), //UNetworkHandler::ResponsePetitionAlarm
    EX_NOTIFY_EXIT_BEAUTYSHOP(NotifyExitBeautyShop::new, ConnectionState.IN_GAME), //UNetworkHandler::NotifyExitBeautyshop
    EX_EVENT_REGISTER_XMAS_WISHCARD(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestRegisterXMasWishCard
    EX_ENCHANT_SCROLL_ITEM_ADD(RequestExAddEnchantScrollItem::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestExAddEnchantScrollItem
    EX_ENCHANT_SUPPORT_ITEM_REMOVE(RequestExRemoveEnchantSupportItem::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestExRemoveEnchantSupportItem
    EX_SELECT_CARD_REWARD(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestCardReward
    EX_DIVIDE_ADENA_START(RequestDivideAdenaStart::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestDivideAdenaStart
    EX_DIVIDE_ADENA_CANCEL(RequestDivideAdenaCancel::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestDivideAdenaCancel
    EX_DIVIDE_ADENA(RequestDivideAdena::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestDivideAdena
    EX_ACQUIRE_POTENTIAL_SKILL(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestAcquireAbilityList
    EX_REQUEST_POTENTIAL_SKILL_LIST(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestAbilityList
    EX_RESET_POTENTIAL_SKILL(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestResetAbilityPoint
    EX_CHANGE_POTENTIAL_POINT(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestChangeAbilityPoint
    EX_STOP_MOVE(RequestStopMove::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestStopMove
    EX_ABILITY_WND_OPEN(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestAbilityWndOpen
    EX_ABILITY_WND_CLOSE(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestAbilityWndClose
    EX_START_LUCKY_GAME(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestLuckyGameStartInfo
    EX_BETTING_LUCKY_GAME(RequestLuckyGamePlay::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestLuckyGamePlay
    EX_TRAININGZONE_LEAVING(null, ConnectionState.IN_GAME), //UNetworkHandler::NotifyTrainingRoomEnd
    EX_ENCHANT_ONE(RequestNewEnchantPushOne::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestNewEnchantPushOne
    EX_ENCHANT_ONE_REMOVE(RequestNewEnchantRemoveOne::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestNewEnchantRemoveOne
    EX_ENCHANT_TWO(RequestNewEnchantPushTwo::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestNewEnchantPushTwo
    EX_ENCHANT_TWO_REMOVE(RequestNewEnchantRemoveTwo::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestNewEnchantRemoveTwo
    EX_ENCHANT_CLOSE(RequestNewEnchantClose::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestNewEnchantClose
    EX_ENCHANT_TRY(RequestNewEnchantTry::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestNewEnchantTry
    EX_ENCHANT_RETRY_TO_PUT_ITEMS(RequestNewEnchantRetryToPutItems::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestNewEnchantRetryToPutItems
    EX_REQUEST_CARD_REWARD_LIST(null, ConnectionState.IN_GAME), //UNetworkHandler::ExRequestCardRewardList
    EX_REQUEST_ACCOUNT_ATTENDANCE_INFO(null, ConnectionState.IN_GAME), //UNetworkHandler::ExRequestAccountAttendanceInfo
    EX_REQUEST_ACCOUNT_ATTENDANCE_REWARD(null, ConnectionState.IN_GAME), //UNetworkHandler::ExRequestAccountAttendanceReward
    EX_TARGET(RequestTargetActionMenu::new, ConnectionState.IN_GAME), //UNetworkHandler::Target
    EX_SELECTED_QUEST_ZONEID(ExSendSelectedQuestZoneID::new, ConnectionState.IN_GAME), //UNetworkHandler::ExSendSelectedQuestZoneID
    EX_ALCHEMY_SKILL_LIST(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestAlchemySkillList
    EX_TRY_MIX_CUBE(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestAlchemyTryMixCube
    REQUEST_ALCHEMY_CONVERSION(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestAlchemyTryMixCube
    EX_EXECUTED_UIEVENTS_COUNT(null, ConnectionState.IN_GAME), //UNetworkHandler::SendExecutedUIEventsCount
    EX_CLIENT_INI(DISCARD, ConnectionState.AUTHENTICATED), //UNetworkHandler::ExSendClientINI
    EX_REQUEST_AUTOFISH(ExRequestAutoFish::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestExAutoFish
    EX_REQUEST_VIP_ATTENDANCE_ITEMLIST(RequestVipAttendanceItemList::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestVipAttendanceItemList
    EX_REQUEST_VIP_ATTENDANCE_CHECK(RequestVipAttendanceCheck::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestVipAttendanceCheck
    EX_TRY_ENSOUL(RequestItemEnsoul::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestItemEnsoul
    EX_CASTLEWAR_SEASON_REWARD(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestCastleWarSeasonReward
    EX_BR_VIP_PRODUCT_LIST_REQ(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestVipProductList
    EX_REQUEST_LUCKY_GAME_INFO(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestVipLuckyGameInfo
    EX_REQUEST_LUCKY_GAME_ITEMLIST(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestVipLuckyGameItemList
    EX_REQUEST_LUCKY_GAME_BONUS(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestVipLuckyGameBonus
    EX_VIP_INFO(null, ConnectionState.IN_GAME), //UNetworkHandler::ExRequestVipInfo
    EX_CAPTCHA_ANSWER(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestCaptchaAnswer
    EX_REFRESH_CAPTCHA_IMAGE(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestRefreshCaptchaImage
    EX_PLEDGE_SIGNIN(RequestPledgeSignInForOpenJoiningMethod::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestPledgeSignInForOpenJoiningMethod
    EX_REQUEST_MATCH_ARENA(null, ConnectionState.IN_GAME), //UNetworkHandler::ExRequestMatchArena
    EX_CONFIRM_MATCH_ARENA(null, ConnectionState.IN_GAME), //UNetworkHandler::ExConfirmMatchArena
    EX_CANCEL_MATCH_ARENA(null, ConnectionState.IN_GAME), //UNetworkHandler::ExCancelMatchArena
    EX_CHANGE_CLASS_ARENA(null, ConnectionState.IN_GAME), //UNetworkHandler::ExChangeClassArena
    EX_CONFIRM_CLASS_ARENA(null, ConnectionState.IN_GAME), //UNetworkHandler::ExConfirmClassArena
    EX_DECO_NPC_INFO(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestOpenDecoNPCUI
    EX_DECO_NPC_SET(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestCheckAgitDecoAvailability
    EX_FACTION_INFO(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestUserFactionInfo
    EX_EXIT_ARENA(null, ConnectionState.IN_GAME), //UNetworkHandler::ExExitArena
    EX_REQUEST_BALTHUS_TOKEN(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestEventBalthusToken
    EX_PARTY_MATCHING_ROOM_HISTORY(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestPartyMatchingHistory
    EX_ARENA_CUSTOM_NOTIFICATION(null, ConnectionState.IN_GAME), //UNetworkHandler::ExArenaCustomNotification
    EX_TODOLIST(RequestTodoList::new, ConnectionState.IN_GAME, ConnectionState.JOINING_GAME), //UNetworkHandler::RequestTodoList
    EX_TODOLIST_HTML(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestTodoListHTML
    EX_ONE_DAY_RECEIVE_REWARD(RequestOneDayRewardReceive::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestOneDayRewardReceive
    EX_QUEUETICKET(null, ConnectionState.IN_GAME),
    EX_PLEDGE_BONUS_UI_OPEN(RequestPledgeBonusOpen::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestPledgeBonusOpen
    EX_PLEDGE_BONUS_REWARD_LIST(RequestPledgeBonusRewardList::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestPledgeBonusRewardList
    EX_PLEDGE_BONUS_RECEIVE_REWARD(RequestPledgeBonusReward::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestPledgeBonusReward
    EX_SSO_AUTHNTOKEN_REQ(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestSSOAuthnToken
    EX_QUEUETICKET_LOGIN(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestQueueTicketLogin
    EX_BLOCK_DETAIL_INFO(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestBlockMemoInfo
    EX_TRY_ENSOUL_EXTRACTION(RequestTryEnSoulExtraction::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestTryEnSoulExtraction
    EX_RAID_BOSS_SPAWN_INFO(RequestRaidBossSpawnInfo::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestRaidBossSpawnInfo
    EX_RAID_SERVER_INFO(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestRaidServerInfo
    EX_SHOW_AGIT_SIEGE_INFO(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestShowAgitSiegeInfo
    EX_ITEM_AUCTION_STATUS(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestItemAuctionStatus
    EX_MONSTER_BOOK_OPEN(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestMonsterBookOpen
    EX_MONSTER_BOOK_CLOSE(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestMonsterBookClose
    EX_REQ_MONSTER_BOOK_REWARD(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestMonsterBookReward
    EX_MATCHGROUP(null, ConnectionState.IN_GAME), //UNetworkHandler::ExRequestMatchGroup
    EX_MATCHGROUP_ASK(null, ConnectionState.IN_GAME), //UNetworkHandler::ExRequestMatchGroupAsk
    EX_MATCHGROUP_ANSWER(null, ConnectionState.IN_GAME), //UNetworkHandler::ExRequestMatchGroupAnswer
    EX_MATCHGROUP_WITHDRAW(null, ConnectionState.IN_GAME), //UNetworkHandler::ExRequestMatchGroupWithdraw
    EX_MATCHGROUP_OUST(null, ConnectionState.IN_GAME), //UNetworkHandler::ExRequestMatchGroupOust
    EX_MATCHGROUP_CHANGE_MASTER(null, ConnectionState.IN_GAME), //UNetworkHandler::ExRequestMatchGroupChangeMaster
    EX_UPGRADE_SYSTEM_REQUEST(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestUpgradeSystemResult
    EX_CARD_UPDOWN_PICK_NUMB(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestCardUpdownGamePickNumber
    EX_CARD_UPDOWN_GAME_REWARD_REQUEST(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestCardUpdownGameReward
    EX_CARD_UPDOWN_GAME_RETRY(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestCardUpdownGameRetry
    EX_CARD_UPDOWN_GAME_QUIT(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestCardUpdownGameQuit
    EX_ARENA_RANK_ALL(null, ConnectionState.IN_GAME), //UNetworkHandler::ExRequestArenaRankAll
    EX_ARENA_MYRANK(null, ConnectionState.IN_GAME), //UNetworkHandler::ExRequestArenaMyRank
    EX_SWAP_AGATHION_SLOT_ITEMS(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestSwapAgathionSlotItems
    EX_PLEDGE_CONTRIBUTION_RANK(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestExPledgeContributionRank
    EX_PLEDGE_CONTRIBUTION_INFO(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestExPledgeContributionInfo
    EX_PLEDGE_CONTRIBUTION_REWARD(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestExPledgeContributionReward
    EX_PLEDGE_LEVEL_UP(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestExPledgeLevelUp
    EX_PLEDGE_MISSION_INFO(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestPledgeMissionInfo
    EX_PLEDGE_MISSION_REWARD(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestPledgeMissionReward
    EX_PLEDGE_MASTERY_INFO(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestExPledgeMasteryInfo
    EX_PLEDGE_MASTERY_SET(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestExPledgeMasterySet
    EX_PLEDGE_MASTERY_RESET(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestExPledgeMasteryReset
    EX_PLEDGE_SKILL_INFO(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestExPledgeSkillInfo
    EX_PLEDGE_SKILL_ACTIVATE(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestExPledgeSkillActivate
    EX_PLEDGE_ITEM_LIST(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestExPledgeItemList
    EX_PLEDGE_ITEM_ACTIVATE(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestExPledgeItemActivate
    EX_PLEDGE_ANNOUNCE(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestExPledgeAnnounce
    EX_PLEDGE_ANNOUNCE_SET(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestExPledgeAnnounceSet
    EX_CREATE_PLEDGE(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestCreatePledge
    EX_PLEDGE_ITEM_INFO(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestExPledgeItemInfo
    EX_PLEDGE_ITEM_BUY(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestExPledgeItemBuy
    EX_ELEMENTAL_SPIRIT_INFO(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestExElementalSpiritInfo
    EX_ELEMENTAL_SPIRIT_EXTRACT_INFO(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestExElementalSpiritExtractInfo
    EX_ELEMENTAL_SPIRIT_EXTRACT(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestExElementalSpiritExtract
    EX_ELEMENTAL_SPIRIT_EVOLUTION_INFO(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestExElementalSpiritEvolutionInfo
    EX_ELEMENTAL_SPIRIT_EVOLUTION(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestExElementalSpiritEvolution
    EX_ELEMENTAL_SPIRIT_SET_TALENT(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestExElementalSpiritSetTalent
    EX_ELEMENTAL_SPIRIT_INIT_TALENT(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestExElementalSpiritInitTalent
    EX_ELEMENTAL_SPIRIT_ABSORB_INFO(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestExElementalSpiritAbsorbInfo
    EX_ELEMENTAL_SPIRIT_ABSORB(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestExElementalSpiritAbsorb
    EX_REQUEST_LOCKED_ITEM(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestExRequestLockedItem
    EX_REQUEST_UNLOCKED_ITEM(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestExRequestUnlockedItem
    EX_LOCKED_ITEM_CANCEL(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestExLockedItemCancel
    EX_UNLOCKED_ITEM_CANCEL(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestExUnLockedItemCancel
    EX_ELEMENTAL_SPIRIT_CHANGE_TYPE(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestExElementalSpiritChangeType
    REQUEST_BLOCK_LIST_FOR_AD(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestBlockListForAD
    REQUEST_USER_BAN_INFO(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestUserBanInfo
    EX_INTERACT_MODIFY(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestInteractModify
    EX_TRY_ENCHANT_ARTIFACT(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestEnchantArtifact
    EX_UPGRADE_SYSTEM_NORMAL_REQUEST(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestUpgradeSystemResultN
    EX_PURCHASE_LIMIT_SHOP_ITEM_LIST(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestExPurchaseLimitShopItemList
    EX_PURCHASE_LIMIT_SHOP_ITEM_BUY(RequestPurchaseLimitShopItemBuy::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestExPurchaseLimitShopItemBuy
    EX_OPEN_HTML(RequestOpenWndWithoutNPC::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestOpenWndWithoutNPC
    EX_REQUEST_CLASS_CHANGE(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestExClassChange
    EX_REQUEST_CLASS_CHANGE_VERIFYING(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestExClassChangeVerifying
    EX_REQUEST_TELEPORT(RequestExRequestTeleport::new, ConnectionState.IN_GAME), //UNetworkHandler::RequestExRequestTeleport
    EX_XIGN_CODE(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestXignCode
    EX_COSTUME_USE_ITEM(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestCostumeUseItem
    EX_COSTUME_LIST(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestCostumeList
    EX_COSTUME_COLLECTION_SKILL_ACTIVE(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestCostumeCollectSkillActive
    EX_COSTUME_EVOLUTION(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestCostumeEvolution
    EX_COSTUME_EXTRACT(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestCostumeExtract
    EX_COSTUME_LOCK(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestCostumeLock
    EX_COSTUME_CHANGE_SHORTCUT(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestCostumeChangeShort
    EX_MAGICLAMP_GAME_INFO(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestMagicLampGameInfo
    EX_MAGICLAMP_GAME_START(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestMagicLampGameStart
    EX_ACTIVATE_AUTO_SHORTCUT(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestExActivateAutoShortcut
    EX_PREMIUM_MANAGER_LINK_HTML(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestPremiumManagerLinkHtml
    EX_PREMIUM_MANAGER_PASS_CMD_TO_SERVER(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestPremiumManagerBypassToServer
    EX_ACTIVATED_CURSED_TREASURE_BOX_LOCATION(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestTreasureBoxLocation
    EX_PAYBACK_LIST(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestExPaybackList
    EX_PAYBACK_GIVE_REWARD(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestExPaybackGiveReward
    EX_AUTOPLAY_SETTING(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestExAutoplaySetting
    EX_FESTIVAL_BM_INFO(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestFestivalInfo
    EX_FESTIVAL_BM_GAME(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestFestivalGame
    EX_GACHA_SHOP_INFO(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestExGachaShopInfo
    EX_GACHA_SHOP_GACHA_GROUP(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestExGachaShopGachaGroup
    EX_GACHA_SHOP_GACHA_ITEM(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestExGachaShopGachaItem
    EX_TIME_RESTRICT_FIELD_LIST(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestExTimeRestrictFieldList
    EX_TIME_RESTRICT_FIELD_USER_ENTER(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestExTimeRestrictFieldUserEnter
    EX_RANKING_CHAR_INFO(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestRankingCharInfo
    EX_RANKING_CHAR_HISTORY(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestRankingCharHistory
    EX_RANKING_CHAR_RANKERS(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestRankingCharRankers
    EX_PLEDGE_MERCENARY_RECRUIT_INFO_SET(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestExPledgeMercenaryRecruitInfoSet
    EX_MERCENARY_CASTLEWAR_CASTLE_INFO(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestExMCWCastleInfo
    EX_MERCENARY_CASTLEWAR_CASTLE_SIEGE_INFO(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestExMCWCastleSiegeInfo
    EX_MERCENARY_CASTLEWAR_CASTLE_SIEGE_ATTACKER_LIST(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestExMCWCastleSiegeAttackerList
    EX_MERCENARY_CASTLEWAR_CASTLE_SIEGE_DEFENDER_LIST(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestExMCWCastleSiegeDefenderList
    EX_PLEDGE_MERCENARY_MEMBER_LIST(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestExPledgeMercenaryMemberList
    EX_PLEDGE_MERCENARY_MEMBER_JOIN(null, ConnectionState.IN_GAME), //UNetworkHandler::RequestExPledgeMercenaryMemberJoin
    EX_PVPBOOK_LIST(null, ConnectionState.IN_GAME), //UNetworkHandler::RequesetExPvpbookList
    EX_PVPBOOK_KILLER_LOCATION(null, ConnectionState.IN_GAME), //UNetworkHandler::RequesetExPvpbookKillerLocation
    EX_PVPBOOK_TELEPORT_TO_KILLER(null, ConnectionState.IN_GAME), //UNetworkHandler::RequesetExPvpbookTeleportToKiller
    EX_LETTER_COLLECTOR_TAKE_REWARD(null, ConnectionState.IN_GAME),
    EX_OLYMPIAD_MY_RANKING_INFO(null, ConnectionState.IN_GAME),
    EX_OLYMPIAD_RANKING_INFO(null, ConnectionState.IN_GAME),
    EX_OLYMPIAD_HERO_AND_LEGEND_INFO(null, ConnectionState.IN_GAME),
    EX_CASTLEWAR_OBSERVER_START(null, ConnectionState.IN_GAME);

    public static final ExIncomingPackets[] PACKET_ARRAY;

    static {
        final short maxPacketId = (short) Arrays.stream(values()).mapToInt(ExIncomingPackets::getPacketId).max().orElse(0);
        PACKET_ARRAY = new ExIncomingPackets[maxPacketId + 1];
        for (ExIncomingPackets incomingPacket : values()) {
            PACKET_ARRAY[incomingPacket.getPacketId()] = incomingPacket;
        }
    }

    private final boolean hasExtension;
    private Supplier<ClientPacket> _incomingPacketFactory;
    private Set<ConnectionState> _connectionStates;

    ExIncomingPackets(Supplier<ClientPacket> incomingPacketFactory, boolean hasExtension, ConnectionState... connectionStates) {
        _incomingPacketFactory = incomingPacketFactory != null ? incomingPacketFactory : () -> null;
        _connectionStates = new HashSet<>(Arrays.asList(connectionStates));
        this.hasExtension = hasExtension;
    }

    ExIncomingPackets(Supplier<ClientPacket> incomingPacketFactory, ConnectionState... connectionStates) {
        this(incomingPacketFactory, false, connectionStates);
    }

    @Override
    public int getPacketId() {
        return ordinal();
    }

    @Override
    public ClientPacket newIncomingPacket() {
        return _incomingPacketFactory.get();
    }

    @Override
    public boolean canHandleState(ConnectionState state) {
        return _connectionStates.contains(state);
    }

    @Override
    public boolean hasExtension() {
        return hasExtension;
    }

    @Override
    public PacketFactory handleExtension(PacketBuffer buffer) {
        if (ordinal() == 0x4E) {
            return handleBookMarkPaket(buffer);
        }
        return NULLABLE_PACKET_FACTORY;
    }

    private PacketFactory handleBookMarkPaket(PacketBuffer packet) {
        return switch (packet.readInt()) {
            case 0 -> new DynamicPacketFactory(RequestBookMarkSlotInfo::new);
            case 1 -> new DynamicPacketFactory(RequestSaveBookMarkSlot::new);
            case 2 -> new DynamicPacketFactory(RequestModifyBookMarkSlot::new);
            case 3 -> new DynamicPacketFactory(RequestDeleteBookMarkSlot::new);
            case 4 -> new DynamicPacketFactory(RequestTeleportBookMark::new);
            case 5 -> new DynamicPacketFactory(RequestChangeBookMarkSlot::new);
            default -> NULLABLE_PACKET_FACTORY;
        };
    }

    @Override
    public Set<ConnectionState> getConnectionStates() {
        return _connectionStates;
    }


    static class DynamicPacketFactory implements PacketFactory {

        private final Supplier<ClientPacket> supplier;

        DynamicPacketFactory(Supplier<ClientPacket> supplier) {
            this.supplier = supplier;
        }

        @Override
        public boolean canHandleState(ConnectionState state) {
            return true;
        }

        @Override
        public ClientPacket newIncomingPacket() {
            return supplier.get();
        }
    }
}
